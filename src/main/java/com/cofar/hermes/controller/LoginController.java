package com.cofar.hermes.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

/**
 *
 * @author hmontes
 * @company COFAR
 */
@ManagedBean
@SessionScoped
public class LoginController {

    protected org.apache.logging.log4j.Logger LOGGER;
    private MenuModel menubar = new DefaultMenuModel();

    @SuppressWarnings("OverridableMethodCallInConstructor")
    public LoginController() {
        // LOGGER = LogManager.getRootLogger();
    }

    /*
    private void generarAccesos(DefaultSubMenu menu,List<VentanasCronos> ventanasCronosList, VentanasCronos ventanaActual){
        for(VentanasCronos ventana: ventanasCronosList){
            if(ventana.getVentanaPadre()!= null 
                    && ventana.getVentanaPadre().getCodVentana() == ventanaActual.getCodVentana()){
                if(ventana.getCantidadDatosRelacionados() > 0 ){
                    DefaultSubMenu subMenu = new DefaultSubMenu(ventana.getNombreVentana());
                    this.generarAccesos(subMenu, ventanasCronosList, ventana);
                    menu.addElement(subMenu);
                }
                else{
                    DefaultMenuItem item = new DefaultMenuItem(ventana.getNombreVentana());
                    item.setUrl(ventana.getUrlVentana());
                    menu.addElement(item);
                }
            }
        }
    }
     */
    public void verificarUsuarioAction() {
        /*
        menubar = new DefaultMenuModel();
        String items[] ={"Dashboard",""};

        for (VentanasCronos ventana : ventanasCronosList) {
            if (ventana.getCodVentana() == 1) {
                DefaultSubMenu submenu = new DefaultSubMenu(ventana.getNombreVentana());
                this.generarAccesos(submenu, ventanasCronosList, ventana);
                this.menubar.addElement(submenu);
            }
        }

        DaoGestiones daoGestiones = new DaoGestiones(LOGGER);
        this.gestionActiva = daoGestiones.listarActivo();
        FacesContext.getCurrentInstance().getExternalContext().redirect(path_redirect);
        */
    }

    //<editor-fold defaultstate="collapsed" desc="getter and setter">
    public MenuModel getMenubar() {
        return menubar;
    }

    public void setMenubar(MenuModel menubar) {
        this.menubar = menubar;
    }
    //</editor-fold>

}
