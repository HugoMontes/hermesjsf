package com.cofar.hermes.util;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.HashMap;
import java.io.Serializable;
import java.util.Calendar;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class GuestPreferences implements Serializable {

    private Map<String, String> themeColors;

    private String theme = "orange";

    private String menuLayout = "static";
    
    private int year=0;

    private boolean orientationRTL;

    @PostConstruct
    public void init() {
        themeColors = new HashMap<String, String>();
        themeColors.put("turquoise", "#00acac");
        themeColors.put("blue", "#2f8ee5");
        themeColors.put("orange", "#efa64c");
        themeColors.put("purple", "#6c76af");
        themeColors.put("pink", "#f16383");
        themeColors.put("light-blue", "#63c9f1");
        themeColors.put("green", "#57c279");
        themeColors.put("deep-purple", "#7e57c2");
        year=Calendar.getInstance().get(Calendar.YEAR);
    }

    public String getTheme() {
        return theme;
    }

    public String getMenuLayout() {
        if (this.menuLayout.equals("static")) {
            return "menu-layout-static";
        } else if (this.menuLayout.equals("overlay")) {
            return "menu-layout-overlay";
        } else if (this.menuLayout.equals("horizontal")) {
            return "menu-layout-static menu-layout-horizontal";
        } else {
            return "menu-layout-static";
        }
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setMenuLayout(String menuLayout) {
        this.menuLayout = menuLayout;
    }

    public Map getThemeColors() {
        return this.themeColors;
    }

    public boolean isOrientationRTL() {
        return orientationRTL;
    }

    public void setOrientationRTL(boolean orientationRTL) {
        this.orientationRTL = orientationRTL;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
    
}
