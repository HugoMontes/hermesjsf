package com.cofar.hermes.util;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;

@ManagedBean
@SessionScoped
public class MenuView implements Serializable{

    private MenuModel model;

    @PostConstruct
    public void init() {
        model = new DefaultMenuModel();
        
        // Home
        DefaultMenuItem item = new DefaultMenuItem("Escritorio");
        item.setIcon("fa fa-tachometer");
        model.addElement(item);
        
        //First submenu
        DefaultSubMenu firstSubmenu = new DefaultSubMenu("Kardex médico", "fa fa-user-md");
        
        item = new DefaultMenuItem("Nuevo médico");
        item.setUrl("http://www.primefaces.org");
        item.setIcon("fa fa-plus");
        firstSubmenu.addElement(item);
        
        item = new DefaultMenuItem("Listar médicos");
        item.setUrl("http://www.primefaces.org");
        item.setIcon("fa fa-list");
        firstSubmenu.addElement(item);
         
        model.addElement(firstSubmenu);
        
        //Second submenu
        DefaultSubMenu secondSubmenu = new DefaultSubMenu("Dynamic Actions", "fa fa-fw fa-sitemap");
 
        item = new DefaultMenuItem("Save");
        item.setIcon("fa fa-bar-chart");
        secondSubmenu.addElement(item);
         
        item = new DefaultMenuItem("Delete");
        item.setIcon("fa fa-trash");
        secondSubmenu.addElement(item);
         
        item = new DefaultMenuItem("Redirect");
        item.setIcon("fa fa-search");
        secondSubmenu.addElement(item);
 
        model.addElement(secondSubmenu);
    }

    public MenuModel getModel() {
        return model;
    }
}
